# README #
Wooncreator wizard prototype

### What is this repository for? ###
Modified wizard for viewing houses for the Tuin van Noord Project.
The slider was made by one of my classmates.
The remainder was a quickly put together prototype done me before the presentation.

You can select the floor you want to see on the top left.
The right room on the 4th floor is clickable and shows the change of the room in detail.

https://stud.hosted.hr.nl/0892682/Wooncreator/