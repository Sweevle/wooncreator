$(document).foundation();

$(document).ready(function () {
    var windowHeight = $(window).height();
    $('#sidebar').height(windowHeight);
    $('#main').height($('#sidebar').height() - $('#slider').height());

    var newWidth = $(window).width() - $('#aside').width() - 2;
    $('#slider').width(newWidth);
});

$('#floors div').click(function(){
    $(this).addClass('active').siblings().removeClass('active');
});

$('#keuken-filler').click(function(){
    $('#sliderthing').show();
});